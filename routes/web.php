<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/welcome', 'HomeController@index')->name('home');


//Route::post('/form/add-step1', 'FormController@AddStep1')->name('user.store');

//Route::resource('/form', 'FormController');
Route::get('/form', 'FormController@index')->name('form.index');
Route::get('/form/show', 'FormController@show')->name('form.show');
Route::get('/form/show/{id}', 'FormController@show')->name('form.show');

Route::get('/form/update-step1', 'FormController@updateStep1');
Route::post('/form/update-step1', 'FormController@postupdateStep1');

Route::get('/form/update-step2', 'FormController@updateStep2');
Route::post('/form/update-step2', 'FormController@postupdateStep2');
Route::post('/form/remove-image', 'FormController@removeImage');

Route::get('/form/update-step3', 'FormController@updateStep3');
Route::post('/form/update-step3', 'FormController@postupdateStep3');

Route::get('/form/update-step4', 'FormController@updateStep4');
Route::post('/form/update-step4', 'FormController@postupdateStep4');

Route::get('/form/update-step5', 'FormController@updateStep5');
Route::post('/form/update-step5', 'FormController@postupdateStep5');

Route::get('/form/update-step6', 'FormController@updateStep6');
Route::post('/form/update-step6', 'FormController@postupdateStep6');

Route::get('/form/update-step7', 'FormController@updateStep7');
Route::post('/form/update-step7', 'FormController@postupdateStep7');

Route::get('/form/update-step8', 'FormController@updateStep8');
Route::post('/form/update-step8', 'FormController@postupdateStep8');

Route::get('/form/update-step9', 'FormController@updateStep9');
Route::post('/form/update-step9', 'FormController@postupdateStep9');

Route::get('/form/update-step10', 'FormController@updateStep10');
Route::post('/form/update-step10', 'FormController@postupdateStep10');

Route::get('/form/update-step11', 'FormController@updateStep11');
Route::post('/form/update-step11', 'FormController@postupdateStep11');

Route::get('/form/update-step12', 'FormController@updateStep12');
Route::post('/form/update-step12', 'FormController@postupdateStep12');

Route::get('/form/update-step13', 'FormController@updateStep13');
Route::post('/form/update-step13', 'FormController@postupdateStep13');

Route::get('/form/update-step14', 'FormController@updateStep14');
Route::post('/form/update-step14', 'FormController@postupdateStep14');

Route::get('/form/update-step15', 'FormController@updateStep15');
Route::post('/form/update-step15', 'FormController@postupdateStep15');

Route::get('/form/update-step16', 'FormController@updateStep16');
Route::post('/form/update-step16', 'FormController@postupdateStep16');

Route::get('/form/update-step17', 'FormController@updateStep17');
Route::post('/form/update-step17', 'FormController@postupdateStep17');

Route::get('/form/update-step18', 'FormController@updateStep18');
Route::post('/form/update-step18', 'FormController@postupdateStep18');

Route::get('/form/update-last', 'FormController@updateLast')->name('form.update-last');

//Route::post('/form/store', 'FormController@store');
Route::any('/form/store', 'FormController@store');


