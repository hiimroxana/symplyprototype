@extends('layouts.app')

@section('content')
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"><h4>A few questions about you.</h4></div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="container">
                                <p>Please enter the name of your employer and the city in which you work.</p>
                                <form action="/symplyprototype/public/form/update-step5" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="employer">Employer Name</label>
                                        <input type="text" value="{{ old('employer', $user->employer ?? null) }}" class="form-control" name="employer">
                                    </div>
                                    <div class="form-group">
                                        <label for="employment_city">Employment City</label>
                                        <input type="text" value="{{ old('employment_city', $user->employment_city ?? null) }}" class="form-control" name="employment_city">
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
