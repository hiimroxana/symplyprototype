@extends('layouts.app')

@section('content')
<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h4>A few questions about your upcoming trip to the United States. </h4></div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="container">
                            <p>Please enter the address in which you plan to stay in the United States.</p>
                            <form action="/symplyprototype/public/form/update-step18" method="post">
                                @csrf
                                <div class="form-group">
                                    {{-- <label for="US_address">Address</label> --}}
                                    <input type="text" value="{{ old('US_address', $user->US_address ?? null) }}" class="form-control" name="US_address">
                                </div>

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
