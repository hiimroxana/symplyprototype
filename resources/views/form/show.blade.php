@extends('layouts.app')

@section('content')
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="container">
                                <h4>{{ $user->name }}'s Information:</h4>
                                <br>
                                    <p> E-mail: {{ $user->email }}</p>
                                    <p> First Name: {{ $user->first_name }}</p>
                            </div>

                            <div class="container">
                                @if ($user->is_employed === 1)
                                    <p>Employer Name: {{ $user->employer }}</p>
                                    <p>City: {{ $user->employment_city }}</p>
                                @elseif ($user->is_employed === 0)
                                    <p>{{ $user->name }} is unemployed.</p>
                                @else
                                    <p></p>
                                @endif
                                </div>
                                    <div class="container">
                                        @if ($user->is_in_school === 1)
                                            <p> School Name: {{ $user->school_name }}</p>
                                            <p> City: {{ $user->school_city }}</p>
                                        @elseif ($user->is_in_school === 0)
                                            <p>{{ $user->name }} is not in school.</p>
                                        @else
                                            <p></p>
                                        @endif
                                        </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="card">
                        <div class="card-body">
                            <div class="container">
                            <h4>Family:</h4>
                            <br>
                                <p>Mother's Name: {{ $user->mother_name }}</p>
                                <p>Mother's Birth Date: {{ $user->mother_birth_date }}</p>
                                <p>Mother's Birth City: {{ $user->mother_birth_city }}</p>
                                <p>Mother's Birth Country: {{ $user->mother_birth_country }}</p>
                                <br>
                                <p>Father's name: {{ $user->father_name }}</p>
                                <p>Father's Birth Date: {{ $user->father_birth_date }}</p>
                                <p>Father's Birth City: {{ $user->father_birth_city }}</p>
                                <p>Father's Birth Country: {{ $user->father_birth_country }}</p>
                            </div>

                            <div class="container">
                                @if ($user->is_married === 1)
                                <br>
                                    <p>Spouse Name: {{ $user->spouse_name }}</p>
                                    <p>Spouse Birth Date: {{ $user->spouse_birth_date }}</p>
                                    <p>Spouse Birth City: {{ $user->spouse_birth_city }}</p>
                                    <p>Spouse Birth Country: {{ $user->spouse_birth_country }}</p>
                                    <p>Spouse Address: {{ $user->spouse_address }}</p>
                                @elseif ($user->is_married === 0)
                                <br>
                                    <p>{{ $user->name }} is not married.</p>
                                @else
                                    <p></p>
                                @endif
                            </div>

                            <div class="container">
                                @if ($user->has_children === 1)
                                <br>
                                    <p>Child Name: {{ $user->child_name }}</p>
                                    <p>Child Birth Date: {{ $user->child_birth_date }}</p>
                                    <p>Child Birth City: {{ $user->child_birth_city }}</p>
                                    <p>Child Birth Country: {{ $user->child_birth_country }}</p>
                                    <p>Child Address: {{ $user->child_address }}</p>
                                @elseif ($user->has_children === 0)
                                <br>
                                    <p>{{ $user->name }} doesn't have children.</p>
                                @else
                                    <p></p>
                                @endif
                            </div>
                        </div>
                    </div>
                            <br><br>
                    <div class="card">
                        <div class="card-body">
                            <h4>Travel History:</h4>
                            <div class="container">
                                <br>
                                @if ($user->has_travelled_to_US === 1)
                                    <p>{{ $user->name }} has been to the United States before.</p>
                                @elseif ($user->has_travelled_to_US === 0)
                                <br>
                                    <p>{{ $user->name }} has never been to the United States.</p>
                                @else
                                    <p></p>
                                @endif
                            </div>

                            <div class="container">

                                @if ($user->held_US_visa === 1)
                                    <p>{{ $user->name }} has held a U.S. visa.</p>
                                @elseif ($user->held_US_visa === 0)
                                    <p>{{ $user->name }} has never held a U.S. visa.</p>
                                @else
                                    <p></p>
                                @endif
                            </div>

                            <div class="container">

                                @if ($user->held_US_SSN === 1)
                                    <p>{{ $user->name }} has held a U.S. social security number.</p>
                                @elseif ($user->held_US_SSN === 0)
                                    <p>{{ $user->name }} has never held a U.S. social security number.</p>
                                @else
                                    <p></p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="card">
                        <div class="card-body">
                            <h4>Visa:</h4>
                            <br>
                            <div class="container">
                            <p>Requested Type of Visa: {{ $user->visa_type }}</p>
                            <p>Interview Location: {{ $user->visa_interview_location }}</p>
                            <p>Approximate Arrival Date: {{ $user->arrival_date }}</p>
                            <p>Approximate Departure Date: {{ $user->departure_date }}</p>
                            <p>United States Address: {{ $user->US_address }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </main>

    @endsection('content')
