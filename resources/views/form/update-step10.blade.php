@extends('layouts.app')

@section('content')
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"><h4>A few questions about your family.</h4></div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="container">
                                <p>Please enter below the full name, date of birth, city of birth, country of birth, and current address for your spouse.</p>
                                <form action="/symplyprototype/public/form/update-step10" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="spouse_name">Spouse Name</label>
                                        <input type="text" value="{{ old('spouse_name', $user->spouse_name ?? null) }}" class="form-control" name="spouse_name">
                                    </div>
                                    <div class="form-group">
                                        <label for="spouse_birth_date">Birth Date</label>
                                        <input type="date" value="{{ old('spouse_birth_date', $user->spouse_birth_date ?? null) }}" class="form-control" name="spouse_birth_date" placeholder="MM/DD/YYYY">
                                    </div>
                                    <div class="form-group">
                                        <label for="spouse_birth_city">Birth City</label>
                                        <input type="text" value="{{ old('spouse_birth_city', $user->spouse_birth_city ?? null) }}" class="form-control" name="spouse_birth_city">
                                    </div>
                                    <div class="form-group">
                                        <label for="spouse_birth_country">Birth Country</label>
                                        <input type="text" value="{{ old('spouse_birth_country', $user->spouse_birth_country ?? null) }}" class="form-control" name="spouse_birth_country">
                                    </div>
                                    <div class="form-group">
                                        <label for="spouse_address">Current Address</label>
                                        <input type="text" value="{{ old('spouse_address', $user->spouse_address ?? null) }}" class="form-control" name="spouse_address">
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
