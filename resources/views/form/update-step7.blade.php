@extends('layouts.app')

@section('content')
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"><h4>A few questions about you.</h4></div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="container">
                                <p>Please enter the name of your school and where you currently attend classes.</p>
                                <form action="/symplyprototype/public/form/update-step7" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="school_name">School</label>
                                        <input type="text" value="{{ old('school_name', $user->school_name ?? null) }}" class="form-control" name="school_name">
                                    </div>
                                    <div class="form-group">
                                        <label for="school_city">City</label>
                                        <input type="text" value="{{ old('school_city', $user->school_city ?? null) }}" class="form-control" name="school_city">
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
