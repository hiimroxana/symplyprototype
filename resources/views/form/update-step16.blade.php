
@extends('layouts.app')

@section('content')
<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h4>A few questions about your upcoming trip to the United States. </h4></div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="container">
                            <form action="/symplyprototype/public/form/update-step16" method="post">
                                @csrf
                                <p>Which type of visa would you like to request?</p>
                                <div class="form-group">
                                    {{-- <label for="visa_type">Visa</label> --}}
                                    <input type="text" value="{{ old('visa_type', $user->visa_type ?? null) }}" class="form-control" name="visa_type">
                                </div>

                                <p>In which embassy or consulate would you like to attend your visa interview?</p>

                                <div class="form-group">
                                    {{-- <label for="visa_interview_location">Location</label> --}}
                                    <input type="text" value="{{ old('visa_interview_location', $user->visa_interview_location ?? null) }}" class="form-control" name="visa_interview_location">
                                </div>

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <button type="submit" class="btn btn-primary">Continue</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection

