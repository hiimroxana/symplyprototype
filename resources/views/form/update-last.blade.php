@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h4>Congratulations! You are done. Thank you again for choosing Symply.</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection
