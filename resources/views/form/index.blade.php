@extends('layouts.app')

@section('content')
<div class="m-5">
<h3 class="mb-3">List of all users & form submissions</h3>
<table class="table" id="full-list">
    <thead>
      <tr>
        <th scope="col">Username</th>
        <th scope="col">E-mail</th>
        <th scope="col">Visa Type</th>
        <th scope="col">See more</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <th scope="row">{{ $user->name }}</th>
        <td>{{ $user->email }}</td>
        <td>{{ $user->visa_type }}</td>
        <td><a href=" {{ route('form.show', $user->id) }}">See more</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection('content')
