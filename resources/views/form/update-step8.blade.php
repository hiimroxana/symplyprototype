@extends('layouts.app')

@section('content')
<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h4>A few questions about your family.</h4></div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="container">
                            <p>For both your mother and your father, please enter below their respective full names, dates of birth, cities of birth, and countries of birth.</p>
                            <form action="/symplyprototype/public/form/update-step8" method="post">
                                @csrf
                                <h4>Mother</h4>
                                <div class="form-group">
                                    <label for="mother_name">Name</label>
                                    <input type="text" value="{{ old('mother_name', $user->mother_name ?? null) }}" class="form-control" name="mother_name">
                                </div>
                                <div class="form-group">
                                    <label for="mother_birth_date">Birth Date</label>
                                    <input type="date" value="{{ old('mother_birth_date', $user->mother_birth_date ?? null) }}" class="form-control" name="mother_birth_date" placeholder="MM/DD/YYYY">
                                </div>
                                <div class="form-group">
                                    <label for="mother_birth_city">Birth City</label>
                                    <input type="text" value="{{ old('mother_birth_city', $user->mother_birth_city ?? null) }}" class="form-control" name="mother_birth_city">
                                </div>
                                <div class="form-group">
                                    <label for="mother_birth_country">Birth Country</label>
                                    <input type="text" value="{{ old('mother_birth_country', $user->mother_birth_country ?? null) }}" class="form-control" name="mother_birth_country">
                                </div>
                                <br>
                                <h4>Father</h4>
                                <div class="form-group">
                                    <label for="father_name">Name</label>
                                    <input type="text" value="{{ old('father_name', $user->father_name ?? null) }}" class="form-control" name="father_name">
                                </div>
                                <div class="form-group">
                                    <label for="father_birth_date">Birth Date</label>
                                    <input type="date" value="{{ old('father_birth_date', $user->father_birth_date ?? null) }}" class="form-control" name="father_birth_date" placeholder="MM/DD/YYYY">
                                </div>
                                <div class="form-group">
                                    <label for="father_birth_city">Birth City</label>
                                    <input type="text" value="{{ old('father_birth_city', $user->father_birth_city ?? null) }}" class="form-control" name="father_birth_city">
                                </div>
                                <div class="form-group">
                                    <label for="father_birth_country">Birth Country</label>
                                    <input type="text" value="{{ old('father_birth_country', $user->father_birth_country ?? null) }}" class="form-control" name="father_birth_country">
                                </div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <button type="submit" class="btn btn-primary">Continue</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
