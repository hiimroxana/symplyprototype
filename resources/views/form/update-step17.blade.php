@extends('layouts.app')

@section('content')
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"><h4>A few questions about your upcoming trip to the United States. </h4></div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="container">
                                <form action="/symplyprototype/public/form/update-step17" method="post">
                                    @csrf
                                    <p>Please pick an approximate date in which you intent to arrive in the United States.</p>
                                    <div class="form-group">
                                        {{-- <label for="arrival_date">Arrival Date</label> --}}
                                        <input type="date" value="{{ old('arrival_date', $user->arrival_date ?? null) }}" class="form-control" name="arrival_date" placeholder="MM/DD/YYYY">
                                    </div>
                                    <p>Please pick an approxiate date in which you intend to depart the United States.</p>
                                    <div class="form-group">
                                        {{-- <label for="departure_date">Departure Date</label> --}}
                                        <input type="date" value="{{ old('departure_date', $user->departure_date ?? null) }}" class="form-control" name="departure_date" placeholder="MM/DD/YYYY">
                                    </div>

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
