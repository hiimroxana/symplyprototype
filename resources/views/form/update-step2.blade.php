@extends('layouts.app')

@section('content')
<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h4>Hello {{ $user->name }}. Let's get started on your visa application.</h4></div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="container">
                            @if(isset($user->passportImg))
                                {{-- Passport Image: --}}
                                <img alt="Passport Image" src="/storage/passportimg/{{$user->passportImg}}"/>
                                @endif
                                <form action="/symplyprototype/public/form/update-step2" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <p>Please upload your passport below.</p>

                                    <div class="form-group">
                                        <input type="file" {{ (!empty($user->passportImg)) ? "disabled" : ''}} class="form-control-file" name="passportimg" id="passportimg" aria-describedby="fileHelp">
                                        <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                                    </div>
                                        <button type="submit" class="btn btn-primary">Continue</button>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </form><br/>
                                @if(isset($user->passportImg))
                                <form action="/symplyprototype/public/form/remove-image" method="post">
                                    {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger">Remove Image</button>
                                </form>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection
