@extends('layouts.app')

@section('content')
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"><h4>A few questions about you.</h4></div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="container">
                                <p>Are you currently enrolled in school?</p>
                                <form action="/symplyprototype/public/form/update-step6" method="post">
                                    @csrf
                                    <div class="form-group">
                                        {{-- <label for="is_in_school">Are you currently enrolled in school?</label> --}}
                                        <label class="radio-inline mr-3"><input type="radio" class="m-2" name="is_in_school" value="1" {{{ (isset($user->is_in_school) && $user->is_in_school == '1') ? "checked" : "" }}} checked> Yes</label>
                                        <label class="radio-inline mr-3"><input type="radio" class="m-2" name="is_in_school" value="0" {{{ (isset($user->is_in_school) && $user->is_in_school == '0') ? "checked" : "" }}}> No</label>
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
