@extends('layouts.app')

@section('content')
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"><h4>A few questions about your family.</h4></div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="container">
                                <p>Are you currently married?</p>
                                <form action="/symplyprototype/public/form/update-step9" method="post">
                                    @csrf
                                    <div class="form-group">
                                        {{-- <label for="is_married">Are you currently married?</label> --}}
                                        <label class="radio-inline mr-3"><input type="radio" class="m-2" name="is_married" value="1" {{{ (isset($user->is_married) && $user->is_married == '1') ? "checked" : "" }}} checked> Yes</label>
                                        <label class="radio-inline mr-3"><input type="radio" class="m-2" name="is_married" value="0" {{{ (isset($user->is_married) && $user->is_married == '0') ? "checked" : "" }}}> No</label>
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
