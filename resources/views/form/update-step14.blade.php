@extends('layouts.app')

@section('content')
<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h4>A few questions about your travel history.</h4></div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="container">
                            <p>Have you ever held a U.S. visa?</p>
                            <form action="/symplyprototype/public/form/update-step14" method="post">
                                @csrf
                                <div class="form-group">
                                    {{-- <label for="held_US_visa">Have you ever held a U.S. visa?</label> --}}
                                    <label class="radio-inline mr-3"><input type="radio" class="m-2" name="held_US_visa" value="1" {{{ (isset($user->held_US_visa) && $user->held_US_visa == '1') ? "checked" : "" }}} checked> Yes</label>
                                    <label class="radio-inline mr-3"><input type="radio" class="m-2" name="held_US_visa" value="0" {{{ (isset($user->held_US_visa) && $user->held_US_visa == '0') ? "checked" : "" }}}> No</label>
                                </div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <button type="submit" class="btn btn-primary">Continue</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
