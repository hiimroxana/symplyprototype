<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // page 1
            $table->string('first_name')->nullable();

            // page 3
            $table->string('address')->nullable();

            // page 4
            $table->string('is_employed')->nullable();

            // if YES page 4.1
            $table->string('employer')->nullable();
            $table->string('employment_city')->nullable();

            // page 5
            $table->string('is_in_school')->nullable();

            // if YES page 5.1
            $table->string('school_name')->nullable();
            $table->string('school_city')->nullable();

            // page 6
            $table->string('mother_name')->nullable();
            $table->date('mother_birth_date')->nullable();
            $table->string('mother_birth_city')->nullable();
            $table->string('mother_birth_country')->nullable();
            $table->string('father_name')->nullable();
            $table->date('father_birth_date')->nullable();
            $table->string('father_birth_city')->nullable();
            $table->string('father_birth_country')->nullable();

            // page 7
            $table->string('is_married')->nullable();

            // if YES page 7.1
            $table->string('spouse_name')->nullable();
            $table->date('spouse_birth_date')->nullable();
            $table->string('spouse_birth_city')->nullable();
            $table->string('spouse_birth_country')->nullable();
            $table->string('spouse_address')->nullable();

            // page 8
            $table->string('has_children')->nullable();

            // if YES page 8.1
            $table->string('child_name')->nullable();
            $table->date('child_birth_date')->nullable();
            $table->string('child_birth_city')->nullable();
            $table->string('child_birth_country')->nullable();
            $table->string('child_address')->nullable();

            // page 9
            $table->string('has_travelled_to_US')->nullable();

            // if YES page 9.1
            $table->string('held_US_visa')->nullable();

            // if YES page 9.2
            $table->string('held_US_SSN')->nullable();

            // page 10
            $table->string('visa_type')->nullable();
            $table->string('visa_interview_location')->nullable();

            // page 11
            $table->date('arrival_date')->nullable();
            $table->date('departure_date')->nullable();

            // page 12
            $table->string('US_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
