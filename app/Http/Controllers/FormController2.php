<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class FormController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form.index', ['users' => User::all()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('id',auth()->user()->id)->first();
        $user = $request->session()->get('user');
        $user->save();
        return redirect('/form/update-last');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return view('form.show', ['user' => User::findOrFail($id)]);
        $user = User::find($id);
        return view('form.show', compact('user'));
    }


        // step 1
        public function updateStep1(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step1',compact('user', $user));
        }

        public function postupdateStep1(Request $request)
        {

            $validatedData = $request->validate([
                'first_name' => 'required',
            ]);
            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            return redirect('/form/update-step2');
        }

        //step2
        public function updateStep2(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step2',compact('user', $user));
        }

        public function postUpdateStep2(Request $request)
        {
            $user = $request->session()->get('user');
            if(!isset($user->passportImg)) {
                $request->validate([
                    'passportimg' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);

                $fileName = "passportImage-" . time() . '.' . request()->passportimg->getClientOriginalExtension();

                $request->passportimg->storeAs('passportimg', $fileName);

                $user = $request->session()->get('user');

                $user->passportImg = $fileName;
                $request->session()->put('user', $user);
            }
            return redirect('/form/update-step3');
        }

        //step 3
        public function updateStep3(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step3',compact('user', $user));
        }

        public function postupdateStep3(Request $request)
        {
            $validatedData = $request->validate([
                'address' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            //dd($request->session()->get('user'));
            return redirect('/form/update-step4');
        }

        // step 4
        public function updateStep4(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step4',compact('user', $user));
        }

        public function postupdateStep4(Request $request)
        {
            $validatedData = $request->validate([
                'is_employed' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }

            if($request->is_employed == 1){
                return redirect('/form/update-step5');
            }else{
                return redirect('/form/update-step6');
            }
        }

        // step 5 *optional
        public function updateStep5(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step5',compact('user', $user));
        }

        public function postupdateStep5(Request $request)
        {

            $validatedData = $request->validate([
                'employer' => 'required',
                'employment_city' => 'required',

            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            return redirect('/form/update-step6');
        }

        // step 6
        public function updateStep6(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step6',compact('user', $user));
        }

        public function postupdateStep6(Request $request)
        {

            $validatedData = $request->validate([
                'is_in_school' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            if($request->is_in_school == 1){
                return redirect('/form/update-step7');
            }else{
                return redirect('/form/update-step8');
            }
        }

        // step 7
        public function updateStep7(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step7',compact('user', $user));
        }

        public function postupdateStep7(Request $request)
        {

            $validatedData = $request->validate([
                'school_name' => 'required',
                'school_city' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            return redirect('/form/update-step8');
        }

        // step 8
        public function updateStep8(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step8',compact('user', $user));
        }

        public function postupdateStep8(Request $request)
        {

            $validatedData = $request->validate([
                'mother_name' => 'required',
                'mother_birth_date' => 'required',
                'mother_birth_city' => 'required',
                'mother_birth_country' => 'required',
                'father_name' => 'required',
                'father_birth_date' => 'required',
                'father_birth_city' => 'required',
                'father_birth_country' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            return redirect('/form/update-step9');
        }

            // step 9
            public function updateStep9(Request $request)
            {
                $user = $request->session()->get('user');
                return view('form.update-step9',compact('user', $user));
            }

            public function postupdateStep9(Request $request)
            {
                $validatedData = $request->validate([
                    'is_married' => 'required',
                ]);

                if(empty($request->session()->get('user'))){
                    $user = User::where('id',auth()->user()->id)->first();
                    $user->fill($validatedData);
                    $request->session()->put('user', $user);
                }else{
                    $user = $request->session()->get('user');
                    $user->fill($validatedData);
                    $request->session()->put('user', $user);
                }
                if($request->is_married == 1){
                    return redirect('/form/update-step10');
                }else{
                    return redirect('/form/update-step11');
                }
            }

            // step 10 * optional
        public function updateStep10(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step10',compact('user', $user));
        }

        public function postupdateStep10(Request $request)
        {
            $validatedData = $request->validate([
                'spouse_name' => 'required',
                'spouse_birth_date' => 'required',
                'spouse_birth_city' => 'required',
                'spouse_birth_country' => 'required',
                'spouse_address' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            return redirect('/form/update-step11');
        }

            // step 11
        public function updateStep11(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step11',compact('user', $user));
        }

        public function postupdateStep11(Request $request)
        {
            $validatedData = $request->validate([
                'has_children' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            if($request->has_children == 1){
                return redirect('/form/update-step12');
            }else{
                return redirect('/form/update-step13');
            }
        }

        // step 12 * optional
        public function updateStep12(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step12',compact('user', $user));
        }

        public function postupdateStep12(Request $request)
        {
            $validatedData = $request->validate([
                'child_name' => 'required',
                'child_birth_date' => 'required',
                'child_birth_city' => 'required',
                'child_birth_country' => 'required',
                'child_address' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            return redirect('/form/update-step13');
        }

        // step 13
        public function updateStep13(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step13',compact('user', $user));
        }

        public function postupdateStep13(Request $request)
        {
            $validatedData = $request->validate([
                'has_travelled_to_US' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            if($request->has_travelled_to_US == 1){
                return redirect('/form/update-step14');
            }else{
                return redirect('/form/update-step16');
            }
        }

        // step 14
        public function updateStep14(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step14',compact('user', $user));
        }

        public function postupdateStep14(Request $request)
        {
            $validatedData = $request->validate([
                'held_US_visa' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            if($request->held_US_visa == 1){
                return redirect('/form/update-step15');
            }else{
                return redirect('/form/update-step16');
            }
        }

        // step 15
        public function updateStep15(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step15',compact('user', $user));
        }

        public function postupdateStep15(Request $request)
        {
            $validatedData = $request->validate([
                'held_US_SSN' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            return redirect('/form/update-step16');
        }

        // step 16
        public function updateStep16(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step16',compact('user', $user));
        }

        public function postupdateStep16(Request $request)
        {
            $validatedData = $request->validate([
                'visa_type' => 'required',
                'visa_interview_location' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            return redirect('/form/update-step17');
        }
        // step 17
        public function updateStep17(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step17',compact('user', $user));
        }

        public function postupdateStep17(Request $request)
        {
            $validatedData = $request->validate([
                'arrival_date' => 'required',
                'departure_date' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            return redirect('/form/update-step18');
        }

        // step 18
        public function updateStep18(Request $request)
        {
            $user = $request->session()->get('user');
            return view('form.update-step18',compact('user', $user));
        }

        public function postupdateStep18(Request $request)
        {
            $validatedData = $request->validate([
                'US_address' => 'required',
            ]);

            if(empty($request->session()->get('user'))){
                $user = User::where('id',auth()->user()->id)->first();
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }else{
                $user = $request->session()->get('user');
                $user->fill($validatedData);
                $request->session()->put('user', $user);
            }
            return redirect('/form/store');
        }

        public function updateLast()
        {
            return view('form.update-last');
        }

        public function removeImage(Request $request)
    {
        $user = $request->session()->get('user');
        $userImg = null;
        return view('form.update-step2',compact('user', $user));
    }
}
