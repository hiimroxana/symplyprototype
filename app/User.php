<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'first_name', 'address', 'passportimg',
        'is_employed', 'employer', 'employment_city',
        'is_in_school', 'school_name', 'school_city',
        'mother_name', 'mother_birth_date', 'mother_birth_city', 'mother_birth_country',
        'father_name', 'father_birth_date', 'father_birth_city', 'father_birth_country',
        'is_married', 'spouse_name', 'spouse_birth_date', 'spouse_birth_city', 'spouse_birth_country', 'spouse_address',
        'has_children', 'child_name', 'child_birth_date', 'child_birth_city', 'child_birth_country', 'child_address',
        'has_travelled_to_US', 'held_US_visa', 'held_US_SSN',
        'visa_type', 'visa_interview_location',
        'arrival_date', 'departure_date', 'US_address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
